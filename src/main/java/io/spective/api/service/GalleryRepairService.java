package io.spective.api.service;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.spective.api.model.RedditPost;
import io.spective.api.model.ResolvedPost;
import io.spective.api.model.ScrapedRedditPost;
import io.spective.api.repository.RedditPostRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class GalleryRepairService {
    private final RedditPostRepository repository;
    private final WebClient webClient = WebClient.create("https://www.reddit.com/comments/{id}/.json");

    public boolean needsRepair(RedditPost redditPost) {
        return redditPost.isGallery() && redditPost.getMediaMetadata() == null;
    }

    public Mono<ResolvedPost> repairIfRequired(ResolvedPost resolvedPost) {
        RedditPost redditPost = resolvedPost.getRedditPost();
        if(!needsRepair(redditPost)){
            return Mono.just(resolvedPost);
        }
        log.info("repairing {}",resolvedPost);
        return webClient.get()
                .uri("", redditPost.getId())
                .retrieve().bodyToFlux(RedditResponse.class)
                .filter(x -> "Listing".equals(x.kind()))//filter to listing responses
                .flatMapIterable(x -> x.data().children())
                .filter(x -> "t3".equals(x.kind()))//filter to post children (t3 type)
                .next()
                .map(x -> {
                    if (x.data().getMediaMetadata() != null) {
                        redditPost.setMediaMetadata(new ArrayList<>(x.data().getMediaMetadata().values()));
                        repository.save(redditPost).doOnError(e->log.error("Error saving repaired reddit post",e));
                    }
                    return resolvedPost;
                });


    }

    record RedditResponse(String kind, RedditListing data) {
    }

    record RedditListing(Object after, int dist, String modhash, @JsonAlias("geo_filter") String geoFilter,
                         Object before,
                         List<RedditListItem> children) {
    }

    record RedditListItem(String kind, ScrapedRedditPost data) {
    }
}

