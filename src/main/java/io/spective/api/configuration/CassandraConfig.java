package io.spective.api.configuration;

import io.spective.api.converter.InstantConverter;
import io.spective.api.converter.TimestampConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.cassandra.core.convert.CassandraCustomConversions;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class CassandraConfig {

    @Bean
    public CassandraCustomConversions customConversions() {

        List<Converter<?, ?>> converters = new ArrayList<>();

        converters.add(new TimestampConverter());
        converters.add(new InstantConverter());

        return new CassandraCustomConversions(converters);
    }
}
