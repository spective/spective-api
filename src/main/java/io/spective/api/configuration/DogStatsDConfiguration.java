package io.spective.api.configuration;

import com.timgroup.statsd.NonBlockingStatsDClientBuilder;
import com.timgroup.statsd.StatsDClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DogStatsDConfiguration {

    @Bean
    public StatsDClient getClient(@Value("${DD_AGENT_HOST}") String datadogHost){
        return new NonBlockingStatsDClientBuilder()
                        .prefix("spective")
                        .hostname(datadogHost)
                        .port(8125).build();
    }
}
