package io.spective.api.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
public class CorsConfiguration implements WebFluxConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        corsRegistry.addMapping("/v1/**")
                .allowedOrigins("http://localhost:4200","https://spective.alexk8s.com")
                .allowedMethods("PUT", "GET", "POST", "OPTIONS")
                .maxAge(3600);
    }
}
