package io.spective.api.collector;

public class CollectorParseException extends RuntimeException{
    public CollectorParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
