package io.spective.api.collector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.lhotari.reactive.pulsar.adapter.MessageResult;
import com.github.lhotari.reactive.pulsar.adapter.ReactiveMessageConsumer;
import com.github.lhotari.reactive.pulsar.adapter.ReactivePulsarClient;
import com.timgroup.statsd.StatsDClient;
import io.spective.api.controller.ContentResolutionController;
import io.spective.api.model.IndexedView;
import io.spective.api.model.RedditPost;
import io.spective.api.model.ResolvedPost;
import io.spective.api.model.ScrapedRedditPost;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.*;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static io.spective.api.util.HostnameUtil.HOSTNAME_TAG;


@Component
@Slf4j
public class PreloadHashCollector {
    private final ReactiveElasticsearchOperations elasticsearchOperations;
    private final ContentResolutionController contentResolutionController;
    private final StatsDClient statsDClient;
    private final ConversionService conversionService;
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Value("${reddit-post.topic}")
    private String[] topic;
    @Value("${pulsar.url}")
    private String url;
    @Value(("${pulsar.token}"))
    private String token;

    @Autowired
    public PreloadHashCollector(ReactiveElasticsearchOperations elasticsearchOperations, ContentResolutionController contentResolutionController, StatsDClient statsDClient, ConversionService conversionService) {
        this.elasticsearchOperations = elasticsearchOperations;
        this.contentResolutionController = contentResolutionController;
        this.statsDClient = statsDClient;
        this.conversionService = conversionService;
    }

    @EventListener(ApplicationReadyEvent.class)
    @SneakyThrows
    public void init() {
        Publisher<List<String>> topSubreddits = getTopSubreddits()
                .next()
                .doOnNext(subreddits -> log.info("Current preload is {}",subreddits))
                .cache(Duration.ofMinutes(10))
                .repeat()
                ;
        ReactiveMessageConsumer<byte[]> reactiveMessageConsumer = getConsumer();
        Flux<Void> consumePublisher = reactiveMessageConsumer.consumeMessages(messageFlux ->
                Flux.zip(messageFlux,topSubreddits)
                        .flatMap(tuple -> {
                            Message<byte[]> message = tuple.getT1();
                            List<String> subreddits = tuple.getT2();
                            Flux<ResolvedPost> searchedPostFlux = resolveRedditPost(message, subreddits)
                                    .map(ResolvedPost::new)
                                    .map(Mono::just)
                                    .map(f->contentResolutionController.resolveContent(f,true).timeout(Duration.ofMinutes(1)))
                                    .orElseGet(Flux::empty)
                                    .doOnNext(this::recordMetrics)
                                    .onErrorContinue((e,o)->log.error("Error preloading content {}",o,e));
                            MessageResult<Void> acknowledge = MessageResult.acknowledge(message.getMessageId());
                            return searchedPostFlux.then(Mono.just(acknowledge)) ;

                        },2)
                        .onErrorContinue((e,o)->log.error("Error processing message to preload {}",o,e))
        ).onErrorContinue((e,o)->log.error("Error consuming messages {}",o,e));
        consumePublisher.subscribe();

    }

    @SneakyThrows
    ReactiveMessageConsumer<byte[]> getConsumer(){
        PulsarClient client = PulsarClient.builder()
                .authentication(AuthenticationFactory.token(token))
                .serviceUrl(url)
                .build();
        ReactivePulsarClient reactivePulsarClient = ReactivePulsarClient.create(client);
        return  reactivePulsarClient.messageConsumer(Schema.BYTES)
                .consumerConfigurer(conf -> conf.topic(topic).subscriptionName("spective-preload-hashes").subscriptionType(SubscriptionType.Failover)
                        .batchReceivePolicy(BatchReceivePolicy.builder().maxNumMessages(200).build()))
                .build();
    }

    private void recordMetrics(ResolvedPost data) {
        RedditPost post = data.getRedditPost();
        String[] tags = {"scrape_host:"+post.getScrapeHost(),HOSTNAME_TAG};
        statsDClient.increment("preload",tags);
        long delay = post.getCreatedTime().until(ZonedDateTime.now(), ChronoUnit.MILLIS);
        statsDClient.histogram("preload.delay",delay,tags);
    }

    Flux<List<String>> getTopSubreddits() {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder()
                .addAggregation(AggregationBuilders.terms("subreddit").field("subreddit").subAggregation(
                        AggregationBuilders.sum("sum_duration").field("duration")
                ))
                .withQuery(new RangeQueryBuilder("viewDate").gt(LocalDateTime.now().minusDays(30)))
                .build();
        return elasticsearchOperations.aggregate(nativeSearchQuery, IndexedView.class).map(aggregation -> {
            ParsedStringTerms terms = (ParsedStringTerms) aggregation;
            return terms.getBuckets().stream().map(MultiBucketsAggregation.Bucket::getKeyAsString).toList();
        });
    }


    Optional<RedditPost> resolveRedditPost(Message<byte[]> messages, List<String> subreddits) {
        try {
            ScrapedRedditPost scrapedRedditPost = objectMapper.readValue(messages.getData(), ScrapedRedditPost.class);
            return Optional.ofNullable(conversionService.convert(scrapedRedditPost, RedditPost.class))
                    .filter(p -> subreddits.contains(p.getSubreddit()))
                    ;
        } catch (IOException e) {
            throw new CollectorParseException("Error parsing data from stream", e);
        }
    }
}
