package io.spective.api.collector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.lhotari.reactive.pulsar.adapter.MessageResult;
import com.github.lhotari.reactive.pulsar.adapter.ReactiveMessageConsumer;
import com.github.lhotari.reactive.pulsar.adapter.ReactivePulsarClient;
import com.timgroup.statsd.StatsDClient;
import io.spective.api.model.RedditPost;
import io.spective.api.model.ScrapedRedditPost;
import io.spective.api.repository.RedditPostRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.index.DocIDMerger;
import org.apache.pulsar.client.api.*;
import org.elasticsearch.ElasticsearchStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.util.retry.Retry;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.StreamSupport;

import static io.spective.api.util.HostnameUtil.HOSTNAME_TAG;

@Component
@Slf4j
public class RedditPostCollector {
    private final RedditPostRepository repository;
    private final ConversionService conversionService;
    private final StatsDClient statsDClient;
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Value("${reddit-post.topic}")
    private String[] topic;
    @Value("${pulsar.url}")
    private String url;
    @Value(("${pulsar.token}"))
    private String token;
    @Value("${pulsar.subscription-name}")
    private String subscriptionName;


    @Autowired
    public RedditPostCollector(RedditPostRepository repository, ConversionService conversionService, StatsDClient statsDClient) {
        this.repository = repository;
        this.conversionService = conversionService;
        this.statsDClient = statsDClient;
    }

    @EventListener(ApplicationReadyEvent.class)
    @SneakyThrows
    public void init() {
        PulsarClient client = PulsarClient.builder()
                .authentication(AuthenticationFactory.token(token))
                .serviceUrl(url)
                .build();
        ReactivePulsarClient reactivePulsarClient = ReactivePulsarClient.create(client);
        ReactiveMessageConsumer<byte[]> reactiveMessageConsumer = reactivePulsarClient.messageConsumer(Schema.BYTES)
                .consumerConfigurer(conf -> conf.topic(topic).subscriptionName(subscriptionName).subscriptionType(SubscriptionType.Failover).batchReceivePolicy(BatchReceivePolicy.builder().maxNumMessages(200).build()))
                .build();
        Flux<Message<byte[]>> consumePublisher = reactiveMessageConsumer.consumeMessages(messageFlux ->
                messageFlux
                        .bufferTimeout(200, Duration.ofSeconds(1))
                        .flatMap(messages ->
                                savePosts(messages).thenMany(Flux.fromIterable(messages))
                                        .map(MessageResult::acknowledgeAndReturn), 1
                        )
                        .retryWhen(Retry.backoff(10000000,Duration.ofSeconds(2)).maxBackoff(Duration.ofSeconds(30)))


        )
                .retryWhen(Retry.backoff(10000000,Duration.ofSeconds(2)).maxBackoff(Duration.ofSeconds(30)));
        consumePublisher.subscribe();

    }


    Flux<RedditPost> savePosts(Iterable<Message<byte[]>> messages) {
        List<RedditPost> posts = StreamSupport.stream(messages.spliterator(), false)
                .map(s -> {
                    try {
                        return objectMapper.readValue(s.getData(), ScrapedRedditPost.class);
                    } catch (IOException e) {
                        throw new CollectorParseException("Error parsing data from stream", e);
                    }
                })
                .map(s -> conversionService.convert(s, RedditPost.class))
                .toList();
        log.debug(posts.stream().map(RedditPost::getId).toList().toString());
        return repository.saveAll(posts).onErrorResume(this::checkForNonTypeError, (t) -> Flux.fromIterable(posts))
                .retryWhen(Retry.backoff(10,Duration.ofSeconds(5)))
                .doOnNext(this::captureStats);
    }

    boolean checkForNonTypeError(Throwable t) {//in es 8 the type is no longer sent which causes a NPE when parsing the response
        if (t.getCause() instanceof ElasticsearchStatusException cause) {
            return cause.status().getStatus() == 200;
        }
        return false;
    }

    private void captureStats(RedditPost post) {
        String[] tags = {"scrape_host:" + post.getScrapeHost(), HOSTNAME_TAG};
        statsDClient.incrementCounter("reddit-post.indexed", tags);
        long indexDelay = post.getCreatedTime().until(post.getIngestTime(), ChronoUnit.MILLIS);
        statsDClient.histogram("reddit-post.indexed-delay", indexDelay, tags);
        long scrapeDelay = post.getCreatedTime().until(post.getScrapeTime(), ChronoUnit.MILLIS);
        statsDClient.histogram("reddit-post.scrape-delay", scrapeDelay, tags);
    }
}
