package io.spective.api.model;

import lombok.Builder;
import lombok.Data;
import lombok.With;

@Data
@Builder
@With
public class ContentMetadata {
    private final String postedUrl;
    private String originalUrl;
    private ContentType type;
    private String origin;
    private String originId;
    private String contentType;
    private String hash;
    private String imageHash;
    private long size;
    private boolean preloaded;
}
