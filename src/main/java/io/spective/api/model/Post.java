package io.spective.api.model;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Objects;

@Data
public class Post {
    //post metadata
    private String redditId;
    private String title;
    private String author;
    private String subreddit;
    private ZonedDateTime createdTime;
    private boolean over18;
    private String source;

    //content metadata
    private String postedUrl;
    private String originalUrl;
    private ContentType type;
    private String origin;
    private String originId;
    private String contentType;
    private String hash;
    private String imageHash;
    private long size;

    public String getId(){
        return Objects.requireNonNullElse(imageHash,hash);
    }

    public String getUrl(){
        return originalUrl;
    }
}
