package io.spective.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.With;


@Data
@With
@AllArgsConstructor
public class ResolvedPost {
    private Post post;
    private RedditPost redditPost;
    private ContentMetadata contentMetadata;
    private Duplicate duplicate = Duplicate.NONE;

    public ResolvedPost(RedditPost redditPost){
        this.redditPost = redditPost;
    }

    @Getter
    @AllArgsConstructor
    public enum Duplicate {
        NONE(false),
        IMAGE_HASH(true),
        HASH(true),
        URL(true);
        private final boolean isDuplicate;
    }
}
