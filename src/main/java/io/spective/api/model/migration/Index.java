package io.spective.api.model.migration;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Index {
    private String name;
    private boolean migrated;
    private long documentCount;
    private String size;
    private boolean hasDumpFile;
    private long alreadyMigratedCount;
    private long bytes;

    public long getMissingCount() {
        return documentCount - alreadyMigratedCount;
    }

}
