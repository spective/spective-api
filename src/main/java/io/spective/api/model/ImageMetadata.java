package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class ImageMetadata {
    @JsonAlias("u")
    private String url;
    @JsonAlias("x")
    private int width;
    @JsonAlias("y")
    private int height;
    private String gif;
    private String mp4;

}
