package io.spective.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Table("view")
@With
@AllArgsConstructor
@NoArgsConstructor
public class View {
    @PrimaryKeyColumn(name = "submission_id", type = PrimaryKeyType.PARTITIONED, ordinal = 1)
    private String id;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 2)
    private String user;
    @PrimaryKeyColumn(name = "view_time", type = PrimaryKeyType.CLUSTERED, ordinal = 3)
    @Column("view_time")
    private LocalDateTime viewDate;


    private Set<String> tags;
    @Column("added_tags")
    private Set<String> addedTags;
    @Column("removed_tags")
    private Set<String> removedTags;
    @Column("alternate_subreddits")
    private Set<String> alternateSubreddits;
    @Column("alternate_titles")
    private Set<String> alternateTitles;
    @Column("copied_by")
    private Set<String> copiedBy;
    private long size;
    @Column("over_18")
    private boolean over18;
    @Column("image_hash")
    private String imageHash;
    private boolean fullscreen;
    private int score;
    @Column("submitted_date")
    private LocalDateTime submittedDate;
    private String url;
    private String source;
    @Column("origin_id")
    private String originId;
    private String poster;
    @Column("original_url")
    private String originalUrl;
    @Column("loaded_date")
    private LocalDateTime loadedDate;
    private int duration;
    private String origin;
    private int version;
    @Column("last_update")
    private LocalDateTime lastUpdated;
    private String subreddit;
    @Column("reddit_id")
    private String redditId;
    private String hash;
    private boolean deleted;
    @Column("posted_url")
    private String postedUrl;
    private String title;
    private String type;
    @Column("tag_version")
    private int tagVersion;

}
