package io.spective.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.time.LocalDateTime;
import java.util.Set;

@Document(indexName = "spective-view")
@Setting(replicas = 0)
@Data
@NoArgsConstructor
public class IndexedView {
    @Field(type = FieldType.Keyword,name = "submission_id")
    private String id;
    @Field(type = FieldType.Keyword)
    private String user;
    @Field(type = FieldType.Date)
    private LocalDateTime viewDate;


    @Field(type = FieldType.Keyword)
    private Set<String> tags;
    @Field(type = FieldType.Keyword)
    private Set<String> addedTags;
    @Field(type = FieldType.Keyword)
    private Set<String> removedTags;
    @Field(type = FieldType.Keyword)
    private Set<String> alternateSubreddits;
    @Field(type = FieldType.Keyword)
    private Set<String> alternateTitles;
    @Field(type = FieldType.Keyword)
    private Set<String> copiedBy;
    private long size;
    private boolean over18;
    @Field(type = FieldType.Keyword)
    private String imageHash;
    private boolean fullscreen;
    private int score;
    @Field(type = FieldType.Date)
    private LocalDateTime submittedDate;
    @Field(type = FieldType.Text)
    private String url;
    @Field(type = FieldType.Keyword)
    private String source;
    @Field(type = FieldType.Keyword)
    private String originId;
    @Field(type = FieldType.Keyword)
    private String poster;
    @Field(type = FieldType.Text)
    private String originalUrl;
    @Field(type = FieldType.Date)
    private LocalDateTime loadedDate;
    private int duration;
    @Field(type = FieldType.Keyword)
    private String origin;
    private int version;
    @Field(type = FieldType.Date)
    private LocalDateTime lastUpdated;
    @Field(type = FieldType.Keyword)
    private String subreddit;
    @Field(type = FieldType.Keyword)
    private String redditId;
    @Field(type = FieldType.Keyword)
    private String hash;
    private boolean deleted;
    @Field(type = FieldType.Text)
    private String postedUrl;
    @Field(type = FieldType.Text)
    private String title;
    @Field(type = FieldType.Keyword)
    private String type;
    private int tagVersion;

    public IndexedView(View view){
        BeanUtils.copyProperties(view,this);
    }
}
