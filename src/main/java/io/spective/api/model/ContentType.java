package io.spective.api.model;

public enum ContentType {
    IMAGE, VIDEO, UNKNOWN, GIF
}
