package io.spective.api.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.time.ZonedDateTime;
import java.util.List;

@Document(indexName = "spective-reddit-post")
@Setting(shards = 20, replicas = 0)
@Data
public class RedditPost {

    @Id
    private String id;
    @MultiField(
            mainField = @Field(type = FieldType.Keyword, includeInParent = true, normalizer = "lowercase"),
            otherFields = {@InnerField(type = FieldType.Search_As_You_Type, suffix = "search")}
    )
    private String author;
    @Field(type = FieldType.Text)
    private String title;
    @Field(type = FieldType.Keyword)
    private String url;
    @MultiField(
            mainField = @Field(type = FieldType.Keyword, includeInParent = true, normalizer = "lowercase"),
            otherFields = {@InnerField(type = FieldType.Search_As_You_Type, suffix = "search")}
    )
    private String subreddit;
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private String source;
    @Field(type = FieldType.Date)
    private ZonedDateTime createdTime; //convert from utc
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private boolean over18;
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private String type;
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private boolean isGallery;
    @Field(index = false, store = true, type = FieldType.Object)
    private List<MediaMetadata> mediaMetadata;
    @Field(type = FieldType.Date)
    private ZonedDateTime scrapeTime; //convert from int
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private String scrapeHost;
    @Field(type = FieldType.Date)
    private ZonedDateTime ingestTime; //convert from string
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private String ingestHost;
    @Field(type = FieldType.Keyword, normalizer = "lowercase")
    private String legacyIndex;
}
