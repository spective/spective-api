package io.spective.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@Table("search_history")
@AllArgsConstructor
public class SearchHistory {

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 1)
    private String user;
    @PrimaryKeyColumn(name = "search_time", type = PrimaryKeyType.CLUSTERED, ordinal = 2)
    private LocalDateTime search_time;
    private String subreddit;
    private String author;
    private String title;
    @Column("safe_only")
    private boolean safeOnly;
}
