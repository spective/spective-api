package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.List;

@Data
public class MediaMetadata {
    @JsonAlias("e")
    private String type;
    private String id;
    @JsonAlias("m")
    private String contentType;
    @JsonAlias("o")
    private List<ImageMetadata> original;
    @JsonAlias("p")
    private List<ImageMetadata> preview;
    @JsonAlias("s")
    private ImageMetadata source;
    private String status;
    private String dashUrl;
    private String hlsUrl;
    @JsonAlias("isGif")
    private boolean isGif;
    @JsonAlias("x")
    private int width;
    @JsonAlias("y")
    private int height;


}
