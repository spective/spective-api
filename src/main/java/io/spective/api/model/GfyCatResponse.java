package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class GfyCatResponse{
    private GfyCatGif gif;
    private GfyCatUser user;

    public static record GfyCatGif(
            String id,
            Instant createDate,
            boolean hasAudio,
            int width,
            int height,
            long likes,
            List<String> tags,
            boolean verified,
            long views,
            double duration,
            boolean published,
            GfyCatUrls urls,
            String userName,
            int type,
            String avgColor
    ){}
    public static record GfyCatUser(
            @JsonProperty("creationtime") Instant creationTime,
            long followers,
            long following,
            long gifs,
            String name,
            String profileImageUrl,
            String profileUrl,
            long publishedGifs,
            long subscription,
            String url,
            String username,
            boolean verified,
            long views
    ){}
    public static record GfyCatUrls(
            String sd,
            String hd,
            String gif,
            String poster,
            String thumbnail,
            String vthumbnail
    ){}
}

