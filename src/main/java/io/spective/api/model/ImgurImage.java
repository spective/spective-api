package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ImgurImage {
    private String id;
    private String title;
    private String description;
    private Date datetime;
    private boolean animated;
    private int width;
    private int height;
    private long size;
    private long views;
    private long bandwidth;
    private String link;
    private String gifv;
    private String mp4;
    @JsonProperty("mp4_size")
    private String mp4Size;
    private boolean nsfw;

}
