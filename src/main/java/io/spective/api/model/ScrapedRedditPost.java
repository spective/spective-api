package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScrapedRedditPost {
    private String author;
    private String title;
    private String url;
    private String id;
    private String subreddit;
    private String source;
    @JsonProperty("created_utc")
    private long createdUtc;
    @JsonProperty("over_18")
    private boolean over18;
    private String type;
    @JsonProperty("is_gallery")
    private boolean isGallery;
    @JsonProperty("media_metadata")
    private Map<String, MediaMetadata> mediaMetadata;
    @JsonProperty("scrape_time")
    private int scrapeTime;
    @JsonProperty("scrape_host")
    private String scrapeHost;
}
