package io.spective.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GfyCatItem {
    private String nsfw;
    private int gatekeeper;
    private String mp4Url;
    private String gifUrl;
    private String webmUrl;
    private String webpUrl;
    private String mobileUrl;
    private String mobilePosterUrl;
    private String extraLemmas;
    private String thumb100PosterUrl;
    private String miniUrl;
    private String gif100px;
    private String miniPosterUrl;
    private String max5mbGif;
    private String title;
    private String max2mbGif;
    private String max1mbGif;
    private String posterUrl;
    private String languageText;
    private int views;
    private String userName;
    private String description;
    private boolean hasTransparency;
    private boolean hasAudio;
    private String likes;
    private String dislikes;
    private String gfyNumber;
    private String gfyId;
    private String gfyName;
    private String avgColor;
    private int width;
    private int height;
    private double frameRate;
    private int numFrames;
    private int gifSize;
    private int mp4Size;
    private int webmSize;
    private int createDate;
    @JsonProperty("content_urls")
    private ContentUrls contentUrls;
    private UserData userData;

    @Data
    public static class ContentUrls {
        private ContentUrl max2mbGif;
        private ContentUrl max1mbGif;
        @JsonProperty("100pxGif")
        private ContentUrl oneHundredPxGif;
        private ContentUrl max5mbGif;
        private ContentUrl largeGif;
        private ContentUrl webp;
        private ContentUrl webm;
        private ContentUrl mobilePoster;
        private ContentUrl mp4;
        private ContentUrl mobile;
    }

        @Data
        public static class ContentUrl {
            private int height;
            private int width;
            private String url;
        }

        @Data
        public static class UserData {
            private String name;
            private String profileImageUrl;
            private String url;
            private String username;
            private int followers;
            private int subscription;
            private int following;
            private String profileUrl;
            private int views;
            private boolean verified;
        }
}
