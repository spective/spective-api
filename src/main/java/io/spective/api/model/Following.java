package io.spective.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@Table("following")
@AllArgsConstructor
public class Following {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String user;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String author;
    @Column("follow_date")
    private LocalDateTime followDate;
    @Column("author_deleted")
    private boolean authorDeleted;
    @Column("last_deleted_check")
    private LocalDateTime lastDeletedCheck;
}
