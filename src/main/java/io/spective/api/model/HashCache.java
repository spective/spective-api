package io.spective.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;

@Table("hash_cache")
@Data
@AllArgsConstructor
public class HashCache {
    @PrimaryKey
    private String url;
    private String hash;
    @Column("image_hash")
    private String imageHash;
    @Column("hash_time")
    private LocalDateTime hashTime;
    private long size;
    @Column("origin_id")
    private String originId;
    @Column("reddit_id")
    private String redditId;
    private String subreddit;
    private Boolean preloaded;
}
