package io.spective.api.model.legacy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.spective.api.model.MediaMetadata;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.util.List;

@Data
@Document(indexName = "reddit-*")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScrapedSubmission {
    private String author;
    private String title;
    private String url;
    private String id;
    private String subreddit;
    private String source;
    @Field("created_utc")
    @JsonProperty("created_utc")
    private long createdUtc;
    @Field("over_18")
    @JsonProperty("over_18")
    private boolean over18;
    private String type;
    @Field("is_gallery")
    @JsonProperty("is_gallery")
    private boolean isGallery;
    @Field("media_metadata")
    @JsonProperty("media_metadata")
    private List<MediaMetadata> mediaMetadata;
    @Field("scrape_time")
    @JsonProperty("scrape_time")
    private int scrapeTime;
    @Field("scrape_host")
    @JsonProperty("scrape_host")
    private String scrapeHost;
    @Field("ingest_time")
    @JsonProperty("ingest_time")
    private String ingestTime;
    @Field("ingest_host")
    @JsonProperty("ingest_host")
    private String ingestHost;


}
