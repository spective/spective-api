package io.spective.api.model;

public record SearchRequest(String subreddit,String author,String title,boolean filterUnsafe) {
}
