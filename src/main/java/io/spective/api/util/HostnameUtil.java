package io.spective.api.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class HostnameUtil {

    public static final String HOSTNAME_TAG;
    public static final String HOSTNAME;

    static {
        String hostname;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            hostname = null;
        }
        HOSTNAME_TAG = "index_host:"+hostname;
        HOSTNAME = hostname;
    }
}
