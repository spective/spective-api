package io.spective.api.util;

import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import io.minio.http.Method;
import io.spective.api.model.Post;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class MinioUtil {

    @Value("${S3_BUCKET}")
    private String s3Bucket;
    @Value("${S3_URL}")
    private String s3Url;
    @Value("${AWS_ACCESS_KEY_ID}")
    private String s3AccessKey;
    @Value("${AWS_SECRET_ACCESS_KEY}")
    private String s3SecretKey;
    @Value("${AWS_S3_ENDPOINT}")
    private String s3Endpoint;

    @SneakyThrows
    public String resolvedSignedGetUrl(Post post){
        MinioClient minioClient = MinioClient.builder().credentials(s3AccessKey, s3SecretKey)
                .endpoint(s3Url)
                .build();
        return minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(s3Bucket)
                        .object("preload/"+post.getId())
                        .expiry(1, TimeUnit.HOURS)
                .build());
    }

    @SneakyThrows
    public void savePreloadedFile(Path filePath, String fileId,String contentType) {
        MinioClient minioClient = MinioClient.builder().credentials(s3AccessKey, s3SecretKey)
                .endpoint(s3Endpoint)
                .build();
        long size = Files.size(filePath);
        try {
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(s3Bucket)
                            .object("preload/"+fileId)
                            .stream(new FileInputStream(filePath.toFile()), size, -1)
                            .contentType(contentType)
                            .build()
            );
            log.debug("Saved preload content {}",fileId);
        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            log.error("Error saving preload content",e);
        }
    }
}
