package io.spective.api.util;

import dev.brachtendorf.jimagehash.hash.Hash;
import dev.brachtendorf.jimagehash.hashAlgorithms.PerceptiveHash;
import lombok.SneakyThrows;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static Mono<String> hashImage(Publisher<DataBuffer> dataStream) {
        return DataBufferUtils.join(dataStream).map(HashUtil::hashImage);
    }

    @SneakyThrows
    private static String hashImage(DataBuffer dataBuffer) {
        InputStream inputStream = dataBuffer.asInputStream();
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        if (bufferedImage == null)
            return "null";
        PerceptiveHash perceptiveHash = new PerceptiveHash(25);
        Hash hash = perceptiveHash.hash(bufferedImage);
        DataBufferUtils.release(dataBuffer);
        return toString(hash.toByteArray());
    }

    public static Mono<String> hash(Publisher<DataBuffer> dataStream) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return Flux.from(dataStream)
                .doOnNext(d -> {
                    digest.update(d.asByteBuffer());
                        DataBufferUtils.release(d);
                })
                .then(Mono.fromSupplier(() -> toString(digest.digest())));
    }

    public static String toString(byte[] hash) {
        char[] hexChars = new char[hash.length * 2];
        for (int j = 0; j < hash.length; j++) {
            int v = hash[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


}
