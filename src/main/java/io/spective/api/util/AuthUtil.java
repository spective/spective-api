package io.spective.api.util;

import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import reactor.core.publisher.Mono;

public class AuthUtil {
    public static Mono<String> getUserId(){
        return ReactiveSecurityContextHolder.getContext().map(s -> s.getAuthentication().getPrincipal()).cast(Jwt.class)
                .map(Jwt::getSubject);
    }
}
