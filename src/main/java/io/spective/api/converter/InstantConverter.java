package io.spective.api.converter;

import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class InstantConverter implements Converter<Timestamp, ZonedDateTime> {
    @Override
    public ZonedDateTime convert(Timestamp source) {
        return ZonedDateTime.of(source.toLocalDateTime(), ZoneId.of("UTC"));
    }
}
