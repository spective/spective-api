package io.spective.api.converter;

import io.spective.api.model.RedditPost;
import io.spective.api.model.legacy.ElasticSubmissionWithBadIngestTime;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

@Component
public class BadLegacySubmissionConverter implements Converter<ElasticSubmissionWithBadIngestTime, RedditPost> {
    @Override
    public RedditPost convert(ElasticSubmissionWithBadIngestTime source) {
        RedditPost redditPost = new RedditPost();
        BeanUtils.copyProperties(source, redditPost);
        redditPost.setCreatedTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getCreatedUtc()), ZoneId.of("UTC")));
        redditPost.setScrapeTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getScrapeTime()), ZoneId.of("UTC")));
        Map<String, Object> timeMap = source.getIngestTime();
        if (timeMap == null) {

            redditPost.setIngestTime(ZonedDateTime.of((Integer) timeMap.get("year"), (Integer) timeMap.get("monthValue"), (Integer) timeMap.get("dayOfMonth"), (Integer) timeMap.get("hour"), (Integer) timeMap.get("minute"), (Integer) timeMap.get("second"), (Integer) timeMap.get("nano"), ZoneId.of("Etc/UTC")));
        } else {
            redditPost.setIngestTime(ZonedDateTime.now());
        }
        if (redditPost.getUrl().length() > 10_000) {
            redditPost.setUrl(redditPost.getUrl().substring(0, 10_000));
        }
        return redditPost;
    }
}
