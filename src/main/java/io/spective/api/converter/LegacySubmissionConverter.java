package io.spective.api.converter;

import io.spective.api.model.RedditPost;
import io.spective.api.model.legacy.ScrapedSubmission;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class LegacySubmissionConverter implements Converter<ScrapedSubmission, RedditPost> {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS][X][XXX]");

    @Override
    public RedditPost convert(ScrapedSubmission source) {
        RedditPost redditPost = new RedditPost();
        BeanUtils.copyProperties(source, redditPost);
        redditPost.setCreatedTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getCreatedUtc()), ZoneId.of("UTC")));
        redditPost.setScrapeTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getScrapeTime()), ZoneId.of("UTC")));
        if (source.getIngestTime() != null) {

            redditPost.setIngestTime(ZonedDateTime.parse(source.getIngestTime(), dateTimeFormatter));
        } else {
            redditPost.setIngestTime(ZonedDateTime.now());
        }
        if (redditPost.getUrl().length() > 10_000) {
            redditPost.setUrl(redditPost.getUrl().substring(0, 10_000));
        }
        return redditPost;
    }
}
