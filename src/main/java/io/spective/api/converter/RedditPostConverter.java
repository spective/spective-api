package io.spective.api.converter;

import io.spective.api.model.RedditPost;
import io.spective.api.model.ScrapedRedditPost;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import static io.spective.api.util.HostnameUtil.HOSTNAME;

@Component
public class RedditPostConverter implements Converter<ScrapedRedditPost, RedditPost> {


    @Override
    public RedditPost convert(ScrapedRedditPost source) {
        RedditPost redditPost = new RedditPost();
        BeanUtils.copyProperties(source, redditPost);
        redditPost.setCreatedTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getCreatedUtc()), ZoneId.of("UTC")));
        redditPost.setScrapeTime(ZonedDateTime.ofInstant(Instant.ofEpochSecond(source.getScrapeTime()), ZoneId.of("UTC")));
        redditPost.setIngestTime(ZonedDateTime.now());
        redditPost.setIngestHost(HOSTNAME);
        if (source.getMediaMetadata() != null) {
            redditPost.setMediaMetadata(new ArrayList<>(source.getMediaMetadata().values()));
        }
        if (redditPost.getUrl().length() > 10_000) {
            redditPost.setUrl(redditPost.getUrl().substring(0, 10_000));
        }
        return redditPost;
    }
}
