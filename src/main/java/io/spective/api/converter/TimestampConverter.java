package io.spective.api.converter;

import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.time.ZonedDateTime;

public class TimestampConverter implements Converter<ZonedDateTime, Timestamp> {
    @Override
    public Timestamp convert(ZonedDateTime source) {
        return Timestamp.valueOf(source.toLocalDateTime());
    }
}
