package io.spective.api.controller;

import io.netty.buffer.PooledByteBufAllocator;
import io.spective.api.contentresolve.ContentResolver;
import io.spective.api.model.*;
import io.spective.api.repository.HashCacheRepository;
import io.spective.api.util.HashUtil;
import io.spective.api.util.MinioUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;
import reactor.util.retry.Retry;

import java.io.IOException;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("v1/content-resolution")
@AllArgsConstructor
@Slf4j
public class ContentResolutionController {
    private final ReactiveElasticsearchOperations elasticsearchOperations;
    private final List<ContentResolver> contentResolvers;
    private final HashCacheRepository hashCacheRepository;
    private final MinioUtil minioUtil;
    private final WebClient webClient = WebClient.create();

    @GetMapping()
    public Flux<ResolvedPost> getContent(@RequestParam String redditId) {
        return resolveContent(elasticsearchOperations.get(redditId, RedditPost.class).map(ResolvedPost::new), false);
    }

    public Flux<ResolvedPost> resolveContent(Publisher<ResolvedPost> publisher, boolean preload) {
        return Flux.from(publisher)
                .flatMapSequential(r -> resolveContent(r, preload), 3);
    }


    private Flux<ResolvedPost> resolveContent(ResolvedPost resolvedPost, boolean preload) {
        if (resolvedPost.getDuplicate().isDuplicate()) {// don't process content if the post has already been marked as a duplicate
            return Flux.just(resolvedPost);
        }
        RedditPost redditPost = resolvedPost.getRedditPost();

        for (ContentResolver contentResolver : contentResolvers) {
            if (contentResolver.canResolveContent(redditPost)) {
                return contentResolver.resolveContent(redditPost)
                        .checkpoint(redditPost.getId())
                        .flatMapSequential(c -> applyHash(c, redditPost, preload), 2)
                        .doOnError(e->log.warn("Received error when resolving content for {}. Will retry up to 5 times",resolvedPost,e))
                        .retryWhen(Retry.backoff(5, Duration.ofSeconds(1)).filter(t -> t instanceof WebClientException || t instanceof SocketException))
                        .map(resolvedPost::withContentMetadata);
            }
        }
        log.debug("Unable to resolve content for {}", redditPost);
        return Flux.empty();
    }

    Mono<ContentMetadata> applyHash(ContentMetadata contentMetadata, RedditPost redditPost, boolean preload) {
        Mono<ContentMetadata> pullHashes = webClient.get()
                .uri(contentMetadata.getOriginalUrl())
                .exchangeToMono(r -> {
                    if (r.statusCode() == HttpStatus.NOT_FOUND) {
                        return Mono.empty();
                    }
                    return resolveContent(contentMetadata,r).flatMap((tuple) -> {
                        Path tempPath = tuple.getT1();
                        ContentMetadata x = tuple.getT2();
                        return Mono.defer(()->{
                        if (preload) {
                            minioUtil.savePreloadedFile(tempPath, Objects.requireNonNullElse(x.getImageHash(), x.getHash()), x.getContentType());
                        } else {
                            try {
                                Files.deleteIfExists(tempPath);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return Mono.just(x);
                        });
                    });
                })
                .doOnNext(metadata -> hashCacheRepository.save(new HashCache(metadata.getOriginalUrl(), metadata.getHash(), metadata.getImageHash(), LocalDateTime.now(), metadata.getSize(), metadata.getOriginId(), redditPost.getId(), redditPost.getSubreddit(), preload))
                        .doOnError(e -> log.error("error saving hash cache", e)).subscribe());
        return hashCacheRepository.findById(contentMetadata.getOriginalUrl())
                .map(cache -> contentMetadata.withHash(cache.getHash()).withImageHash(cache.getImageHash()).withSize(cache.getSize()).withPreloaded(Objects.requireNonNullElse(cache.getPreloaded(), false)))
                .switchIfEmpty(pullHashes);
    }

    /**
     * Write the content from the client response to a file. Hashes that content. And applies the hashes to the ContentMetadata
     *
     * @param contentMetadata the ContentMetadata source that the hashes from the content will be applied to
     * @param clientResponse  the response that the content will be downloaded from
     * @return the path of the file that the content was written to and the ContentMetadata with the hashes added
     */
    Mono<Tuple2<Path, ContentMetadata>> resolveContent(ContentMetadata contentMetadata, ClientResponse clientResponse) {
        contentMetadata.setSize(clientResponse.headers().contentLength().orElse(0));
        contentMetadata.setContentType(clientResponse.headers().contentType().map(MediaType::toString).orElse("image/webp"));
        Path tempPath = Path.of("/tmp", UUID.randomUUID().toString());
        Mono<Void> writeFileMono = DataBufferUtils.write(clientResponse.body(BodyExtractors.toDataBuffers()), tempPath, StandardOpenOption.CREATE_NEW);
        Flux<DataBuffer> dataBufferFlux = Flux.defer(() -> DataBufferUtils.read(tempPath, new NettyDataBufferFactory(PooledByteBufAllocator.DEFAULT), Short.MAX_VALUE));
        if (contentMetadata.getType() == ContentType.IMAGE) {
            Mono<String> hash = dataBufferFlux
                    .transform(HashUtil::hash).next();
            Mono<String> hashImage = dataBufferFlux
                    .transform(HashUtil::hashImage).next();
            return writeFileMono.then(Mono.zip(hash, hashImage, (h, hi) -> contentMetadata.withHash(h).withImageHash(hi)))
                    .map(c -> Tuples.of(tempPath, c));
        } else {
            Mono<String> hash = dataBufferFlux
                    .transform(d -> DataBufferUtils.takeUntilByteCount(d, 100_000_000))
                    .transform(HashUtil::hash).next();
            return writeFileMono.then(hash).map(h -> {
                contentMetadata.setHash(h);
                return contentMetadata;
            }).map(c -> Tuples.of(tempPath, c));
        }
    }
}
