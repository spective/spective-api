package io.spective.api.controller;

import io.spective.api.model.IndexedView;
import io.spective.api.model.Post;
import io.spective.api.model.View;
import io.spective.api.repository.IndexedViewRepository;
import io.spective.api.repository.ViewRepository;
import io.spective.api.util.AuthUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Controller
@AllArgsConstructor
@Slf4j
public class ViewController {

    private final ViewRepository viewRepository;
    private final IndexedViewRepository indexedViewRepository;

    @MessageMapping("view/get")
    public Mono<View> getView(Post post){
        return AuthUtil.getUserId()
                .flatMap(user->viewRepository.findFirstByIdAndUser(post.getId(),user));

    }

    record ViewRequest(Post post,int duration,boolean fullscreen){}

    @MessageMapping("view")
    public Mono<Void> saveView(ViewRequest request){
        Post post = request.post;
        View view = new View();
        BeanUtils.copyProperties(post,view);
        view.setType(post.getType().toString());
        view.setLoadedDate(post.getCreatedTime().toLocalDateTime());
        view.setViewDate(LocalDateTime.now());
        view.setPoster(post.getAuthor());
        view.setDuration(request.duration);
        view.setFullscreen(request.fullscreen);
        return ReactiveSecurityContextHolder.getContext().map(s->s.getAuthentication().getPrincipal()).cast(Jwt.class)
                .map(jwt->view.withUser(jwt.getSubject()))
                .flatMap(viewRepository::save)
                .map(IndexedView::new)
                .flatMap(indexedViewRepository::save)
                .then();

    }
}
