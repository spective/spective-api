package io.spective.api.controller;

import io.spective.api.model.HashCache;
import io.spective.api.repository.HashCacheRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RequestMapping("/v1/hash-cache")
@RestController
@AllArgsConstructor
public class HashCacheController {

    private HashCacheRepository hashCacheRepository;

    @GetMapping
    public Mono<ResponseEntity<List<HashCache>>> getHashCache(@RequestParam(required = false) String hash, @RequestParam(required = false) String imageHash){
        if(StringUtils.hasText(imageHash)){
            return hashCacheRepository.findByImageHash(imageHash).collectList().map(ResponseEntity::ok);
        }
        if(StringUtils.hasText(hash)){
            return hashCacheRepository.findByHash(hash).collectList().map(ResponseEntity::ok);
        }
        return Mono.just(ResponseEntity.notFound().build());
    }

}
