package io.spective.api.controller;

import io.spective.api.model.Following;
import io.spective.api.repository.FollowingRepository;
import io.spective.api.util.AuthUtil;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@RequestMapping(value = "/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@AllArgsConstructor
public class FollowingController {
    private final FollowingRepository followingRepository;

    @GetMapping("following")
    public Flux<Following> getFollowing() {
        return AuthUtil.getUserId()
                .flatMapMany(followingRepository::findByUser);
    }

    @PostMapping("follow")
    public Mono<Following> follow(@RequestBody String author) {
        return AuthUtil.getUserId()
                .map(user -> new Following(user, author, LocalDateTime.now(), false, null))
                .flatMap(followingRepository::save);
    }

    @PostMapping("unfollow")
    public Mono<Void> unfollow(@RequestBody String author) {
        return AuthUtil.getUserId()
                .flatMap(user -> followingRepository.deleteByUserAndAuthor(user, author));
    }
}
