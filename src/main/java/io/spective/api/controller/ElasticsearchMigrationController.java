package io.spective.api.controller;

import com.datastax.astra.sdk.AstraClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import datadog.trace.api.Trace;
import io.spective.api.model.RedditPost;
import io.spective.api.model.ScrapedRedditPost;
import io.spective.api.model.legacy.ElasticSubmissionWithBadIngestTime;
import io.spective.api.model.legacy.ScrapedSubmission;
import io.spective.api.model.migration.Index;
import io.spective.api.model.migration.MigrationResult;
import io.spective.api.repository.MigrationResultRepository;
import io.spective.api.repository.RedditPostRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.mapping.MappingException;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.BaseStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/v1/migration/elasticsearch")
@AllArgsConstructor
@Slf4j
public class ElasticsearchMigrationController {

    private static final String CURRENT_MIGRATION_VERSION = "14";

    private final ReactiveElasticsearchOperations elasticsearchOperations;
    private final MigrationResultRepository migrationResultRepository;
    private final ConversionService conversionService;
    private final RedditPostRepository redditPostRepository;
    private final AstraClient astraClient;
    private final WebClient webClient = WebClient.builder().build();
    private final Map<String, Long> lineNumberCache = new HashMap<>();


    @GetMapping("indexes")
    @SneakyThrows
    public Flux<Index> getIndexes() {
        Mono<List<String>> indexDetails = webClient.get().uri(URI.create("http://192.168.1.200:9200/_cat/indices/reddit-*")).exchangeToMono(r -> r.bodyToMono(String.class))
                .map(s -> Arrays.stream(s.split("\n")).collect(Collectors.toList()));
        Mono<List<MigrationResult>> migrationResults = migrationResultRepository.findByVersion(CURRENT_MIGRATION_VERSION).collectList().publish(Function.identity());
        return Flux.combineLatest(indexDetails, migrationResults, this::resolveIndexes)
                .flatMap(Flux::fromIterable)
                .concatWith(migrationResults.map(this::resolveGaps).flux().flatMap(Flux::fromStream).take(100))
                .sort(Comparator.comparing(Index::getBytes).reversed());

    }

    private List<Index> resolveIndexes(List<String> indexDetails, List<MigrationResult> migrations) {
        Map<String, MigrationResult> migrationIndexMap = migrations.stream().collect(Collectors.toMap(MigrationResult::getIndex, Function.identity()));
        return indexDetails.stream().map(d -> {
                    var ds = d.split("\\s+");
                    String n = ds[2], s = ds[8];
                    var c = Long.parseLong(ds[6]);
                    boolean migrated = migrationIndexMap.containsKey(n) && migrationIndexMap.get(n).getRecordCount() >= c;
                    boolean h = Files.exists(getDumpFilePath(n));
                    return new Index(n, migrated, c, s, h, migrationIndexMap.getOrDefault(n, new MigrationResult()).getRecordCount(), 0);
                })
                .sorted(Comparator.comparing(Index::getName))
                .collect(Collectors.toList());
    }

    @SneakyThrows
    private Stream<Index> resolveGaps(List<MigrationResult> migrations) {
        Map<String, MigrationResult> migrationIndexMap = migrations.stream().collect(Collectors.toMap(MigrationResult::getIndex, Function.identity()));
        return Files.list(Path.of("gaps"))
                .filter(p -> !p.getFileName().toString().contains("queued"))
                .map(path -> {

                    long bytes = FileUtils.sizeOf(path.toFile());
                    String size = FileUtils.byteCountToDisplaySize(bytes);
                    long lines = lineNumberCache.computeIfAbsent(path.toString(), s -> {
                        try {
                            log.debug("counting {}", path);
                            return Files.lines(path).count();
                        } catch (IOException e) {
                            e.printStackTrace();
                            throw new RuntimeException("error reading file", e);
                        }
                    });
                    String name = path.getFileName().toString();
                    boolean migrated = migrationIndexMap.containsKey(name) && migrationIndexMap.get(name).getRecordCount() >= lines;
                    return new Index(name, migrated, lines, size, true, migrationIndexMap.getOrDefault(name, new MigrationResult()).getRecordCount(), bytes);
                })
                .filter(x -> !x.isMigrated());
    }

    private Path getDumpFilePath(String index) {
        return Path.of("dumpfiles", "dump-" + index + ".json");
    }

    @GetMapping("indexes/{index}")
    public Mono<List<ScrapedSubmission>> getSubmissions(@PathVariable String index) {
        CriteriaQuery query = new CriteriaQuery(new Criteria("_index").is(index));
        query.setPageable(Pageable.ofSize(500));
        return elasticsearchOperations.search(query, ScrapedSubmission.class)
                .take(10, true)
                .map(SearchHit::getContent)
                .collectList();
    }

    @GetMapping("auto-migrate")
    public Flux<MigrationResult> autoMigrate() {
        Mono<List<MigrationResult>> migrationResults = migrationResultRepository.findByVersion(CURRENT_MIGRATION_VERSION).collectList().publish(Function.identity());
        AtomicLong totalRowCount = new AtomicLong();
        return migrationResults.map(this::resolveGaps).flux()
                .flatMap(Flux::fromStream)
                .filter(i -> i.getBytes() > 0 && i.getBytes() < 10_000_000)
                .takeWhile(i -> totalRowCount.getAndAdd(i.getDocumentCount()) < 10_000_000)
                .flatMap(i -> migrate(i).last())
                .subscribeOn(Schedulers.single())
                ;
    }

    @MessageMapping("migrate-index")
    @Trace
    public Flux<MigrationResult> migrate(Index index) {
        Mono<MigrationResult> migrationResultMono = migrationResultRepository.findByVersionAndIndex(CURRENT_MIGRATION_VERSION, index.getName())
                .switchIfEmpty(Mono.just(new MigrationResult(CURRENT_MIGRATION_VERSION, index.getName(), LocalDateTime.now(), null, 0, null)));
        Path dumpFilePath = getDumpFilePath(index.getName());
        Path gapFile = Path.of("gaps", index.getName());
        if (Files.exists(dumpFilePath)) {
            return Flux.from(migrationResultMono).flatMap(x -> migrateFromFile(dumpFilePath, x, ScrapedSubmission.class));
        } else if (Files.exists(gapFile)) {
            return Flux.from(migrationResultMono).flatMap(x -> migrateFromFile(gapFile, x, ScrapedRedditPost.class));
        }
        return Flux.from(migrationResultMono).flatMap(this::migrate);
    }

    private Mono<MigrationResult> saveMigrationResult(MigrationResult migrationResult) {
        String endPointTable = astraClient.getStargateClient().apiRest().keyspace("spective").table("migration_result").getEndPointTable();
        return webClient.post()
                .uri(URI.create(endPointTable))
                .header("x-cassandra-token", astraClient.getToken().orElseThrow())
                .bodyValue(migrationResult)
                .exchangeToMono(r -> {
                    if (r.statusCode().isError()) {
                        return r.bodyToMono(String.class).doOnNext(c -> {
                            log.error("Error saving migration result {} {}", c, migrationResult);
                            throw new RuntimeException("Error saving migration result");
                        }).thenReturn(migrationResult);
                    }
                    return Mono.just(migrationResult);
                })
                .thenReturn(migrationResult);
    }

    @SneakyThrows
    private Flux<MigrationResult> migrateFromFile(Path dumpFilePath, MigrationResult migrationResult, Class<?> targetClass) {
        AtomicBoolean saveError = new AtomicBoolean(false);
        ObjectMapper objectMapper = JsonMapper.builder()
                .findAndAddModules()
                .build();
        AtomicLong counter = new AtomicLong(migrationResult.getRecordCount());
        Flux<MigrationResult> publish = Flux.using(() -> Files.lines(dumpFilePath),
                        Flux::fromStream,
                        BaseStream::close)
                .skip(migrationResult.getRecordCount())
                .takeWhile(x -> !saveError.get())
                .flatMap(this.parseSubmission(targetClass, objectMapper, migrationResult.getIndex()))
                .map(s -> conversionService.convert(s, RedditPost.class))
                .doOnNext(p -> p.setLegacyIndex(migrationResult.getIndex()))
                .buffer(100)
                .flatMap(redditPostRepository::saveAll, 5)
                .map(c -> migrationResult.withRecordCount(counter.incrementAndGet()).withLastId(c.getId()))
                .sample(Duration.ofMillis(50))
                .map(c -> c.withEndTime(LocalDateTime.now()))
                .publish()
                .autoConnect(2);
        publish
                .subscribeOn(Schedulers.single())
                .sample(Duration.ofSeconds(1))
                .checkpoint()
                .flatMap(this::saveMigrationResult)
                .log()
                .doOnError(x -> saveError.set(true))
                .subscribe();
        return Flux.concat(Mono.just(migrationResult), publish);

    }

    private Function<String, Mono<Object>> parseSubmission(Class<?> targetClass, ObjectMapper objectMapper, String index) {
        return s -> {

            try {
                return Mono.just(objectMapper.readValue(s, targetClass));
            } catch (JsonProcessingException e) {
                if (e.getMessage().contains("Unexpected character ('{'")) {
                    log.warn("Invalid character found\n{}", s);
                    return Mono.empty();
                }
                try {
                    return Mono.just(objectMapper.readValue(s, ElasticSubmissionWithBadIngestTime.class));
                } catch (JsonProcessingException f) {
                    throw new RuntimeException("Error parsing submissions in index " + index, f);
                }

            }
        };
    }


    private CriteriaQuery resolveQuery(MigrationResult previousResult) {
        CriteriaQuery query;
        if (previousResult.getLastId() != null) {
            log.info("Index {} had a previous id of {}, starting migration from there", previousResult.getIndex(), previousResult.getLastId());
            query = new CriteriaQuery(new Criteria("_index").is(previousResult.getIndex()).and("id.keyword").greaterThan(previousResult.getLastId()));
        } else {
            log.info("Index {} had no previous id", previousResult.getIndex());
            query = new CriteriaQuery(new Criteria("_index").is(previousResult.getIndex()));
        }
        query.setScrollTime(Duration.ofMinutes(2));
        query.addSort(Sort.by("id.keyword"));
        return query;
    }

    private Flux<MigrationResult> migrate(MigrationResult initialResult) {

        AtomicLong counter = new AtomicLong(initialResult.getRecordCount());
        return getPostsFromElasticsearch(initialResult, ScrapedSubmission.class).buffer(100)
                .flatMap(redditPostRepository::saveAll)
                .map(c -> initialResult.withRecordCount(counter.incrementAndGet()).withLastId(c.getId()))
                .sample(Duration.ofMillis(1000))
                .log()
                .map(c -> c.withEndTime(LocalDateTime.now()))
                .retry(5)
                .flatMap(this::saveMigrationResult)
                .publish()
                .autoConnect();
    }

    private Flux<RedditPost> getPostsFromElasticsearch(MigrationResult initialResult, Class<?> targetClass) {

        CriteriaQuery query = resolveQuery(initialResult);
        return elasticsearchOperations.search(query, targetClass)
                .map(s -> conversionService.convert(s.getContent(), RedditPost.class))
                .doOnNext(p -> p.setLegacyIndex(initialResult.getIndex()))
                .onErrorResume(MappingException.class, t -> getPostsFromElasticsearch(initialResult, ElasticSubmissionWithBadIngestTime.class))
                ;
    }


}
