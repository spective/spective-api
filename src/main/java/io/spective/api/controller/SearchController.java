package io.spective.api.controller;

import com.timgroup.statsd.StatsDClient;
import datadog.trace.api.GlobalTracer;
import datadog.trace.api.interceptor.MutableSpan;
import datadog.trace.api.interceptor.TraceInterceptor;
import io.spective.api.model.*;
import io.spective.api.repository.SearchHistoryRepository;
import io.spective.api.service.GalleryRepairService;
import io.spective.api.util.MinioUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.*;
import org.reactivestreams.Publisher;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.ws.rs.BadRequestException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

@RequestMapping(value = "v1/search", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
@Slf4j
public class SearchController {
    static {
        // In a class static block to avoid initializing multiple times.
        GlobalTracer.get().addTraceInterceptor(new TraceInterceptor() {
            @Override
            public Collection<? extends MutableSpan> onTraceComplete(Collection<? extends MutableSpan> trace) {
                return trace.stream()
                        .peek(MutableSpan::getLocalRootSpan)
                        .filter(s -> !"GET /rsocket".contentEquals(s.getResourceName()))
                        .toList();
            }

            @Override
            public int priority() {
                return 200;  // Some unique number
            }
        });
    }

    private final ContentResolutionController contentResolutionController;
    private final ReactiveElasticsearchOperations elasticsearchOperations;
    private final GalleryRepairService galleryRepairService;
    private final SearchHistoryRepository searchHistoryRepository;
    private final WebClient webClient = WebClient.create();
    private final MinioUtil s3PreloadSigner;
    private final StatsDClient statsDClient;

    @GetMapping
    public Flux<ResolvedPost> searchPosts(@RequestParam(required = false) String subreddit, @RequestParam(required = false) String redditId) {
        Publisher<ResolvedPost> redditPostPublisher;
        if (redditId != null) {
            redditPostPublisher = elasticsearchOperations.get(redditId, RedditPost.class).map(ResolvedPost::new).flatMap(galleryRepairService::repairIfRequired);
        } else if (subreddit != null) {
            redditPostPublisher = Flux.from(getRedditPostResults(subreddit, null, null, false, Duration.of(10, ChronoUnit.YEARS)))
                    .map(searchHitProcessor())
                    .flatMap(galleryRepairService::repairIfRequired);
        } else {
            throw new BadRequestException("Subreddit is required");
        }
        return
                contentResolutionController.resolveContent(redditPostPublisher, false)
                        .flatMapSequential(this::resolvePost, 3)
                        .map(duplicateHashProcessor())
                        .take(50);
    }

    @MessageMapping("search")
    public Flux<ResolvedPost> searchPosts(SearchRequest searchRequest) {
        Mono<SearchHistory> searchHistoryMono = ReactiveSecurityContextHolder.getContext().map(s -> s.getAuthentication().getPrincipal()).cast(Jwt.class)
                .map(jwt -> new SearchHistory(jwt.getSubject(), LocalDateTime.now(), searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe()))
                .flatMap(searchHistoryRepository::save);
        Publisher<SearchHit<RedditPost>>[] publishers;
        if (StringUtils.hasText(searchRequest.author()) || StringUtils.hasText(searchRequest.title())) { //todo: when the database allows reduce number of variants and difference between smaller and larger expected result sets
            publishers = new Publisher[]{
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(1)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(7)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(30)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(10_000))
            };
        } else {
            publishers = new Publisher[]{
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofHours(1)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(1)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(7)),
                    getRedditPostResults(searchRequest.subreddit(), searchRequest.author(), searchRequest.title(), searchRequest.filterUnsafe(), Duration.ofDays(30))
            };
        }
        Flux<ResolvedPost> postFlux = Flux.concat(publishers)
                .map(searchHitProcessor()) //remove duplicate urls
                .limitRate(10)
                .flatMapSequential(galleryRepairService::repairIfRequired)
                .transform(f -> contentResolutionController.resolveContent(f, false))
                .flatMapSequential(this::resolvePost, 3)
                .map(duplicateHashProcessor())
                .doOnNext(this::captureMetrics)
                .doOnError(e -> log.error("Error performing search", e))
                .take(1000);
        return Flux.combineLatest(postFlux, searchHistoryMono, (p, s) -> p);
    }

    Function<SearchHit<RedditPost>, ResolvedPost> searchHitProcessor() {
        Set<String> urls = new HashSet<>();
        return searchHit -> {
            ResolvedPost resolvedPost = new ResolvedPost(searchHit.getContent());
            return urls.add(resolvedPost.getRedditPost().getUrl()) ? resolvedPost :
                    resolvedPost.withDuplicate(ResolvedPost.Duplicate.URL);
        };
    }

    Function<ResolvedPost, ResolvedPost> duplicateHashProcessor() {
        Set<String> hashes = new HashSet<>();
        return resolvedPost -> {
            if (resolvedPost.getDuplicate().isDuplicate()) {
                return resolvedPost;
            }
            if (!hashes.add(resolvedPost.getContentMetadata().getImageHash())) {
                return resolvedPost.withDuplicate(ResolvedPost.Duplicate.IMAGE_HASH);
            }
            if (!hashes.add(resolvedPost.getContentMetadata().getHash())) {
                return resolvedPost.withDuplicate(ResolvedPost.Duplicate.HASH);
            }
            return resolvedPost;
        };
    }

    void captureMetrics(ResolvedPost resolvedPost) {
        ContentMetadata contentMetadata = resolvedPost.getContentMetadata();
        String postTypeTag = contentMetadata != null ? "post-type:" + contentMetadata.getType().toString() : "";
        statsDClient.increment("search.resolved-post", "duplicate:" + resolvedPost.getDuplicate().isDuplicate(), postTypeTag);
        if (resolvedPost.getDuplicate().isDuplicate()) {
            statsDClient.increment("search.resolved-post.duplicate", "type:" + resolvedPost.getDuplicate().toString(), postTypeTag);
        }
    }

    private Publisher<SearchHit<RedditPost>> getRedditPostResults(String subreddit, String author, String title, boolean safeOnly, Duration timeAgo) {
        Criteria criteria;
        if (StringUtils.hasText(subreddit)) {
            criteria = Criteria.where("subreddit").is(subreddit);
            if (StringUtils.hasText(author)) {
                criteria.and(Criteria.where("author").is(author));
            }
        } else if (StringUtils.hasText(author)) {
            criteria = Criteria.where("author").is(author);
        } else {
            throw new BadRequestException("You must provide either and author or subreddit in the request");
        }
        ZonedDateTime now = ZonedDateTime.now();
        criteria.and(Criteria.where("createdTime").between(now.minus(timeAgo), now));
        if (StringUtils.hasText(title)) {
            criteria.and(Criteria.where("title").is(title));
        }
        if (safeOnly) {
            criteria.and(Criteria.where("over18").is(false));
        }
        CriteriaQuery query = new CriteriaQuery(criteria).addSort(Sort.by("createdTime").descending());
        query.setMaxResults(1000);
        return elasticsearchOperations.search(query, RedditPost.class);

    }

    @GetMapping("typeahead/{searchType}")
    public Mono<List<String>> typeahead(@RequestParam String query, @RequestParam(required = false) boolean safeOnly, @PathVariable SearchType searchType) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(new RangeQueryBuilder("createdTime").gt(LocalDateTime.now().minusDays(30)));
        queryBuilder.must(new MultiMatchQueryBuilder(query, searchType.getSearchFields()).type(MultiMatchQueryBuilder.Type.BOOL_PREFIX));
        if (safeOnly) {
            queryBuilder.must(new TermQueryBuilder("over18", false));
        }
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(Pageable.ofSize(1000))
                .build();
        return elasticsearchOperations.search(nativeSearchQuery, RedditPost.class).map(SearchHit::getContent).map(searchType.getFieldAccessor())
                .distinct()
                .take(10)
                .collectList();
    }

    private Mono<ResolvedPost> resolvePost(ResolvedPost resolvedPost) {
        if (resolvedPost.getDuplicate().isDuplicate()) {
            return Mono.just(resolvedPost);
        }
        Post post = new Post();
        BeanUtils.copyProperties(resolvedPost.getContentMetadata(), post);
        BeanUtils.copyProperties(resolvedPost.getRedditPost(), post);
        post.setRedditId(resolvedPost.getRedditPost().getId());
        post.setSource("SEARCH");
        resolvedPost.setPost(post);
        String signedUrl = s3PreloadSigner.resolvedSignedGetUrl(post);
        if (resolvedPost.getContentMetadata().isPreloaded()) {
            post.setOriginalUrl(signedUrl);
            statsDClient.increment("preload.cache", "state:hash-cache");
            return Mono.just(resolvedPost);
        }

        return webClient.get().uri(signedUrl).exchangeToMono(clientResponse -> {
            if (clientResponse.statusCode().is2xxSuccessful()) {
                statsDClient.increment("preload.cache", "state:hit");
                post.setOriginalUrl(signedUrl);
            } else {
                log.info("{} cache miss with status code {}", post.getId(), clientResponse.statusCode());
                statsDClient.increment("preload.cache", "state:miss");
            }
            return Mono.just(resolvedPost);
        });
    }

    @Getter
    @AllArgsConstructor
    public enum SearchType {
        SUBREDDIT(new String[]{"subreddit.search*"}, "subreddit", RedditPost::getSubreddit),
        AUTHOR(new String[]{"author.search*"}, "author", RedditPost::getAuthor);

        private final String[] searchFields;
        private final String aggregateField;
        private final Function<RedditPost, String> fieldAccessor;
    }
}
