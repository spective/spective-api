package io.spective.api.repository;

import io.spective.api.model.SearchHistory;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchHistoryRepository extends ReactiveCrudRepository<SearchHistory,String> {
}
