package io.spective.api.repository;

import io.spective.api.model.migration.MigrationResult;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MigrationResultRepository extends ReactiveCrudRepository<MigrationResult, String> {
    Flux<MigrationResult> findByVersion(String version);

    Mono<MigrationResult> findByVersionAndIndex(String version, String index);
}
