package io.spective.api.repository;

import io.spective.api.model.View;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ViewRepository extends ReactiveCrudRepository<View, String> {
    Mono<View> findFirstByIdAndUser(String id, String user);
}
