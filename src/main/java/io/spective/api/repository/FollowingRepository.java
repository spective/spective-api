package io.spective.api.repository;

import io.spective.api.model.Following;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface FollowingRepository extends ReactiveCrudRepository<Following,String> {
    Flux<Following> findByUser(String user);
    Mono<Void> deleteByUserAndAuthor(String user, String author);
}
