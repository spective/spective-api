package io.spective.api.repository;

import io.spective.api.model.IndexedView;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndexedViewRepository extends ReactiveElasticsearchRepository<IndexedView,String> {
}
