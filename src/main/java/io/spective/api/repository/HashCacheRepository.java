package io.spective.api.repository;

import io.spective.api.model.HashCache;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface HashCacheRepository extends ReactiveCassandraRepository<HashCache,String> {
    Flux<HashCache> findByHash(String hash);
    Flux<HashCache> findByImageHash(String imageHash);
}
