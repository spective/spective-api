package io.spective.api.repository;

import io.spective.api.model.RedditPost;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedditPostRepository extends ReactiveElasticsearchRepository<RedditPost, String> {
}
