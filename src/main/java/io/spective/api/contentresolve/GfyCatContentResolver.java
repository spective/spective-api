package io.spective.api.contentresolve;

import io.spective.api.model.ContentMetadata;
import io.spective.api.model.GfyCatItem;
import io.spective.api.model.RedditPost;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.Set;

@Service
public class GfyCatContentResolver implements ContentResolver {

    static final Set<HttpStatus> MISSING_CONTENT_CODES = Set.of(HttpStatus.NOT_FOUND, HttpStatus.GONE, HttpStatus.FORBIDDEN);
    private final WebClient webClient = WebClient.create("https://api.gfycat.com/v1/gfycats/");

    @Override
    public boolean canResolveContent(RedditPost redditPost) {
        return redditPost.getUrl().contains("gfycat.com");
    }

    @Override
    public Flux<ContentMetadata> resolveContent(RedditPost redditPost) {
        return webClient.get()
                .uri("{id}", resolveOriginId(redditPost.getUrl()))
                .retrieve()
                .bodyToMono(GfyCatItem.class)
                .onErrorResume(WebClientResponseException.class, ex -> MISSING_CONTENT_CODES.contains(ex.getStatusCode()) ? Mono.empty() : Mono.error(ex))
                .map(g -> resolveMetadata(g, redditPost))
                .filter(Optional::isPresent)
                .map(Optional::get).flux();
    }

    Optional<ContentMetadata> resolveMetadata(GfyCatItem gfyItem, RedditPost redditPost) {
        if (gfyItem == null || gfyItem.getContentUrls() == null)
            return Optional.empty();
        String originalUrl;
        if (gfyItem.getFrameRate() < 20 && gfyItem.getContentUrls().getMobile() != null) {
            originalUrl = gfyItem.getContentUrls().getMobile().getUrl();
        } else if (gfyItem.getContentUrls().getMp4().getUrl() != null) {
            originalUrl = gfyItem.getMp4Url();
        } else {
            originalUrl = gfyItem.getContentUrls().getLargeGif().getUrl();
        }
        ContentMetadata contentMetadata = ContentMetadata.builder()
                .type(resolveContentType(originalUrl))
                .origin("GFY_CAT")
                .postedUrl(redditPost.getUrl())
                .originalUrl(originalUrl)
                .originId(gfyItem.getGfyId())
                .build();
        return Optional.of(contentMetadata);
    }
}
