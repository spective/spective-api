package io.spective.api.contentresolve;

import io.spective.api.model.ContentMetadata;
import io.spective.api.model.RedditPost;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.List;

@Component
public class SimpleContentResolver implements ContentResolver {
    private static final List<String> SIMPLE_DOMAINS = List.of("i.redd.it", "i.reddituploads.com");

    @Override
    public boolean canResolveContent(RedditPost redditPost) {
        return SIMPLE_DOMAINS.stream().anyMatch(redditPost.getUrl()::contains);
    }

    @Override
    public Flux<ContentMetadata> resolveContent(RedditPost redditPost) {
        String url = redditPost.getUrl();
        String origin;
        if (url.contains("i.redd.it")) {
            origin = "I_REDDIT";
        } else if (url.contains("i.reddituploads.com")) {
            origin = "I_REDDIT_UPLOADS";
        } else {
            origin = "OTHER";
        }
        ContentMetadata contentMetadata = ContentMetadata.builder()
                .origin(origin)
                .originId(resolveOriginId(url))
                .originalUrl(url)
                .postedUrl(url)
                .type(resolveContentType(url))
                .build();
        return Flux.just(contentMetadata);
    }
}
