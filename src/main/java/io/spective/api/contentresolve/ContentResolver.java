package io.spective.api.contentresolve;

import io.spective.api.model.ContentMetadata;
import io.spective.api.model.ContentType;
import io.spective.api.model.RedditPost;
import reactor.core.publisher.Flux;

import java.util.List;

import static io.spective.api.model.ContentType.*;


public interface ContentResolver {
    List<String> IMAGE_TYPES = List.of("jpg", "jpeg", "png");
    List<String> GIF_TYPES = List.of("gif");
    List<String> VIDEO_TYPES = List.of("mp4", "webm", "gifv");

    default String resolveOriginId(String url) {
        String[] split = url.split("/");
        return split[split.length - 1].split("[.?]")[0];
    }

    default ContentType resolveContentType(String url) {
        int queryIndex = url.indexOf("?");
        int end = queryIndex == -1 ? url.length() : queryIndex;
        String path = url.substring(url.indexOf("/"), end);
        if (IMAGE_TYPES.stream().anyMatch(path::endsWith))
            return IMAGE;
        if (GIF_TYPES.stream().anyMatch(path::endsWith))
            return GIF;
        if (VIDEO_TYPES.stream().anyMatch(path::endsWith))
            return VIDEO;
        return UNKNOWN;
    }

    boolean canResolveContent(RedditPost redditPost);

    Flux<ContentMetadata> resolveContent(RedditPost redditPost);
}
