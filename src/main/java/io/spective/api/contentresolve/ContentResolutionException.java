package io.spective.api.contentresolve;

public class ContentResolutionException extends RuntimeException{
    public ContentResolutionException(String s) {
       super(s);
    }
}
