package io.spective.api.contentresolve;

import io.spective.api.model.ContentMetadata;
import io.spective.api.model.ImgurImage;
import io.spective.api.model.RedditPost;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static org.springframework.util.StringUtils.hasText;

@Component
@Slf4j
public class ImgurContentResolver implements ContentResolver {
    private final WebClient webClient;
    private final List<String> imgurClientIds;
    private final Random random;

    @Autowired
    public ImgurContentResolver(@Value("${imgur.clientid}") List<String> imgurClientIds) {
        this.webClient = WebClient.builder().baseUrl("https://api.imgur.com/3/").build();
        this.random = new Random();
        this.imgurClientIds = Objects.requireNonNullElse(imgurClientIds, Collections.emptyList());
        if (this.imgurClientIds.isEmpty()) {
            log.warn("No client ids found for imgur, please set the \"imgur.clientid\" property to be able to load imgur images");
        }

    }

    ImgurContentResolver(WebClient mockWebclient){ //constructor for tests
       this.webClient = mockWebclient;
       this.imgurClientIds=List.of("");
       this.random = new Random();
    }

    @Override
    public boolean canResolveContent(RedditPost redditPost) {
        if (imgurClientIds.isEmpty()) {
            return false;
        }
        return redditPost.getUrl().contains("imgur.com") && hasText(resolveOriginId(redditPost.getUrl()));
    }

    @Override
    public Flux<ContentMetadata> resolveContent(RedditPost redditPost) {
        if (imgurClientIds.isEmpty()) {
            return Flux.empty();
        }
        if (redditPost.getUrl().contains("/a/")) {
            return resolveAlbumContent(redditPost);
        }
        return resolveImageContent(redditPost);
    }

    Flux<ContentMetadata> resolveImageContent(RedditPost redditPost) {
        String id = resolveOriginId(redditPost.getUrl());
        return webClient.get()
                .uri("image/{id}", id)
                .header("Authorization", "Client-ID " + getClientId())
                .retrieve()
                .bodyToMono(ImgurImageWrapper.class)
                .onErrorResume(WebClientResponseException.class, ex -> ex.getStatusCode() == HttpStatus.NOT_FOUND ? Mono.empty() : Mono.error(ex))
                .map(ImgurImageWrapper::getData)
                .flux()
                .map(i -> parseImage(i, redditPost));

    }

    Flux<ContentMetadata> resolveAlbumContent(RedditPost redditPost) {
        String id = resolveOriginId(redditPost.getUrl()).split("#")[0];
        return webClient.get()
                .uri("album/{id}/images", id)
                .header("Authorization", "Client-ID " + getClientId())
                .retrieve()
                .bodyToMono(ImgurImageListWrapper.class)
                .onErrorResume(WebClientResponseException.class, ex -> ex.getStatusCode() == HttpStatus.NOT_FOUND ? Mono.empty() : Mono.error(ex))
                .flatMapIterable(ImgurImageListWrapper::getData)
                .map(i -> parseImage(i, redditPost));

    }

    String getClientId() {
        return imgurClientIds.get(random.nextInt(imgurClientIds.size()));
    }

    ContentMetadata parseImage(ImgurImage imgurImage, RedditPost redditPost) {
        return ContentMetadata.builder()
                .origin("IMGUR")
                .originId(imgurImage.getId())
                .originalUrl(imgurImage.getLink())
                .type(resolveContentType(imgurImage.getLink()))
                .postedUrl(redditPost.getUrl())
                .build();
    }

    @Data
    private static class ImgurImageWrapper {
        private ImgurImage data;
    }

    @Data
    private static class ImgurImageListWrapper {
        private List<ImgurImage> data;
    }

}
