package io.spective.api.contentresolve;

import io.spective.api.model.ContentMetadata;
import io.spective.api.model.GfyCatResponse;
import io.spective.api.model.RedditPost;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static io.spective.api.contentresolve.GfyCatContentResolver.MISSING_CONTENT_CODES;

@Service
public class RedGifsContentResolver implements ContentResolver {
    private final WebClient webClient = WebClient.builder().baseUrl("https://api.redgifs.com/v2/gifs/").build();

    @Override
    public boolean canResolveContent(RedditPost redditPost) {
        return redditPost.getUrl().contains("redgifs.com");
    }

    @Override
    public Flux<ContentMetadata> resolveContent(RedditPost redditPost) {
        return webClient.get()
                .uri("{id}", resolveOriginId(redditPost.getUrl()))
                .retrieve()
                .bodyToMono(GfyCatResponse.class)
                .map(g -> resolveMetadata(g, redditPost))
                .onErrorResume(WebClientResponseException.class, ex -> MISSING_CONTENT_CODES.contains(ex.getStatusCode()) ? Mono.empty() : Mono.error(ex))
                .flux();
    }

    private ContentMetadata resolveMetadata(GfyCatResponse g, RedditPost redditPost) {
        String hdUrl = g.getGif().urls().hd();
        return ContentMetadata.builder()
                .type(resolveContentType(hdUrl))
                .origin("RED_GIFS")
                .postedUrl(redditPost.getUrl())
                .originalUrl(hdUrl)
                .originId(g.getGif().id())
                .build();
    }
}
