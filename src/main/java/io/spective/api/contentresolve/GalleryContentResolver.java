package io.spective.api.contentresolve;

import io.spective.api.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class GalleryContentResolver implements ContentResolver {
    @Override
    public boolean canResolveContent(RedditPost redditPost) {
        return redditPost.isGallery() && redditPost.getMediaMetadata() != null;
    }

    @Override
    public Flux<ContentMetadata> resolveContent(RedditPost redditPost) {
        List<ContentMetadata> contentMetadataList = new ArrayList<>();
        for (MediaMetadata mediaMetadata : redditPost.getMediaMetadata()) {
            if (mediaMetadata.getStatus().equals("valid")) {
                ContentMetadata contentMetadata = ContentMetadata.builder()
                        .originId(mediaMetadata.getId())
                        .type(resolveContentType(mediaMetadata))
                        .postedUrl(redditPost.getUrl())
                        .originalUrl(resolveOriginalUrl(mediaMetadata).replaceAll("&amp;","&"))
                        .origin("REDDIT_GALLERY")
                        .build();
                contentMetadataList.add(contentMetadata);
            }
        }
        return Flux.fromIterable(contentMetadataList);
    }

    String resolveOriginalUrl(MediaMetadata mediaMetadata) {
        if (mediaMetadata.getSource().getUrl() != null) {
            return mediaMetadata.getSource().getUrl();
        }
        else if ("image/gif".equals(mediaMetadata.getContentType()) &&mediaMetadata.getSource().getGif()!=null){
            return mediaMetadata.getSource().getGif();
        } else if(mediaMetadata.getSource().getMp4()!=null){
            return mediaMetadata.getSource().getMp4();
        }
        Comparator<ImageMetadata> comparing = Comparator.comparing(m -> m.getWidth() * m.getHeight());
        if(mediaMetadata.getOriginal()!=null){
            return mediaMetadata.getOriginal()
                    .stream()
                    .sorted(comparing.reversed())
                    .map(ImageMetadata::getUrl)
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElseThrow();
        }
        if(mediaMetadata.getPreview()!=null){
            return mediaMetadata.getPreview()
                    .stream()
                    .sorted(comparing.reversed())
                    .map(ImageMetadata::getUrl)
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElseThrow();
        }
        throw new ContentResolutionException("Unable to find url for metadata "+mediaMetadata);

    }

    private ContentType resolveContentType(MediaMetadata mediaMetadata) {
        switch (mediaMetadata.getType()) {
            case "Image":
                return ContentType.IMAGE;
            case "AnimatedImage":
                return ContentType.GIF;
            default:
                log.warn("Unknown content type {} - {}", mediaMetadata.getContentType(), mediaMetadata.getType());
                return ContentType.UNKNOWN;
        }
    }

}
