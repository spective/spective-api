package io.spective.api.contentresolve;

import io.spective.api.model.RedditPost;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImgurContentResolverTest {

    @Mock
    private WebClient webClient;
    private ImgurContentResolver resolver;

    @BeforeEach
    void setUp(){
        resolver = Mockito.spy(new ImgurContentResolver(webClient));
    }

    @Test
    void canResolveContent_withValidUrl() {
        RedditPost redditPost = Mockito.mock(RedditPost.class);

       List<String> urls = List.of("https://imgur.com/image/imageId.jpg",
                "https://imgur.com/image/imageId.png",
                "https://imgur.com/image/imageId.gif",
                "https://imgur.com/imageId.mp4",
                "https://imgur.com/iamgeId.webm",
                "https://i.imgur.com/imageId.png",
                "https://slug.imgur.com/imageId.png");
        for (String url : urls) {
            when(redditPost.getUrl()).thenReturn(url);
            assertTrue(resolver.canResolveContent(redditPost),"Should be able to resolve content for "+url);
        }
    }
    @Test
    void canResolveContent_withInvalidUrl() {

        RedditPost redditPost = Mockito.mock(RedditPost.class);
        List<String> urls = List.of("https://imgurnt.com/image/imageId.jpg",
                "https://slug.imgur.com/all/?third_party=1");
        for (String url : urls) {
            when(redditPost.getUrl()).thenReturn(url);
            assertFalse(resolver.canResolveContent(redditPost),"Should be able to resolve content for "+url);
        }
    }
}